# Shazam en python !

L'**objectif** de ce mini-projet est de developper, de manière très incrémentale, une reproduction *simple* de [Shazam](https://www.shazam.com/) en python en mode console ou avec une interface graphique et peut-être web mais nous ne ferons pas ici de developpement mobile.


![shazam](./Images/shazam.png)

Au travers de ce projet, vous decouvrirez les bases de l'audio en python avec des frameworks comme [`pyaudio`](https://pypi.org/project/PyAudio/ ) ou [`dejavu`](https://github.com/worldveil/dejavu), les bases des interfaces graphiques en python et un peu de développement web si vous avez le temps. L'objectif est aussi de vous initer, au travers de ce projet, à plusieurs principes du mouvement dit du [*Software Craftmanship*](https://www.octo.com/fr/publications/20-culture-code). 


## Avant propos - Shazam : comment ça marche ?
Pour ce projet, avant de commencer tout travail de programmation, il est nécessaire de se familiariser un peu avec le fonctionnement de [Shazam](https://en.wikipedia.org/wiki/Shazam_(application)) et de le décortiquer.

Pour cela, et bien, il faut se documenter. 
Par exemple, sans rentrer de manière excessive dans la théorie, le principe est très bien expliqué sur ce [site](https://www.lesnumeriques.com/audio/magie-shazam-dans-entrailles-algorithme-a2375.html) et illustré sur l'image ci-dessous:

![PrincipeShazam](./Images/principe.jpg)

D'autres documentations sont disponibles ici :

 + [https://www.toptal.com/algorithms/shazam-it-music-processing-fingerprinting-and-recognition](https://www.toptal.com/algorithms/shazam-it-music-processing-fingerprinting-and-recognition)
 + [https://en.wikipedia.org/wiki/Acoustic_fingerprint](https://en.wikipedia.org/wiki/Acoustic_fingerprint)
 + et d'autres que vous pouvez chercher par vous même



Pour reproduire le coeur de Shazam, il est donc nécessaire de :
 
  + Construire l'[**empreinte**](https://en.wikipedia.org/wiki/Acoustic_fingerprint) (**acoustic_fingerprint**) d'un morceau de musique ce qui nécessite de d'abord construire son [**spectrogramme**](https://en.wikipedia.org/wiki/Spectrogram) (**spectrogram**), de l'analyser pour en extraire des **pics** (**peaks**)  qui servent alors à générer l'empreinte unique d'un signal audio à l'aide d'une fonction de hachage (vous verrez ce  que sont les fonctions de hachage très bientôt dans le cours d'algorithmique).
 + Ce projet est un projet de programmmation et non de traitement du signal et pour toute cette partie d'analyse du signal et de création des empreintes acoustiques des morceaux, nous utiliserons une bibiothèque existante, développée en python par [Will Devro](http://willdrevo.com/fingerprinting-and-audio-recognition-with-python/), la bibliothèque [`dejavu`](https://github.com/worldveil/dejavu) disponible sur Github.

 
L'objectif de ce projet est donc de programmer un système de reconnaissance audio à partir de cette bibliothèque, puis sur la base de ce système de construire une application, pouvant être une application web, proposant un ensemble de services, d'usages autour de cette fonctionnalité de reconnaissance musicale.

Pour ce projet, nous proposons sur la base de ce service de reconnaissance de développer une application qui prend en entrée un morceau de musique (venant de la bibliothèque musicale de l'utilisateur) ou d'un micro, et qui sur la base du résultat de la reconnaissance, propose une liste de morceaux *similaires* à écouter à l'utilisateur. On peut définir la notion de *similarité* comme on le souhaite ici.  
 
 
  
 
 
 






## Organisation du mini-projet

Ce mini-projet est découpé en plusieurs objectifs, eux-même découpés en  **sprints** et **fonctionnalités**. La notion de sprint fait référence à la [méthode agile](https://fr.wikipedia.org/wiki/M%C3%A9thode_agile). Un sprint correspond à un intervalle de temps pendant lequel l’équipe projet va compléter un certain nombre de tâches.

Ce travail de découpage a été fait pour vous mais c'est une des premières étapes à faire pour tout projet de developpement logiciel, au moins de manière macroscopique. Pensez-y la semaine prochaine !

### **Objectif 1 (MVP): Une système de reconnaissance de fichiers audio avec `dejavu`  (JOUR 1)** 

L'objectif de cette première journée est de constuire et d'implémenter le produit minimal pour notre projet c'est-à-dire le système de reconnaissance ausio. Il s'agit du **[MVP (Minimum Viable product)](https://medium.com/creative-wallonia-engine/un-mvp-nest-pas-une-version-simplifi%C3%A9e-de-votre-produit-89017ac748b0)**.

Ce concept de MVP a été introduit par Eric Ries, l'auteur de [The Lean Startup](http://theleanstartup.com/), une approche spécifique du démarrage d'une activité économique et du lancement d'un produit. La figure ci-dessous permet de bien expliquer ce concept.

![MVP](./Images/mvp.png)

 + **Sprint 0** :
	 + [Installation du socle technique.](./Sprint0Install.md)
	 + [Analyse des besoins.](./Sprint0Analyse.md) 
	 + [Refexion autour de la conception.](./Sprint0Conception.md)

 + **Sprint 1 : Installation et utilisation de [dejavu](https://github.com/worldveil/dejavu)**  
 
 	+ [**Fonctionnalité 1** :Installation de dejavu.](./S1_installdejavu.md)
 	+ [**Fonctionnalité 2** : Reconnaissance de fichiers audio sur disque.](./S1_dejavuuse.md)
 	+ [**Fonctionnalité 3** : Prise en compte du micro.](./S1_micro.md)

 + **Sprint 2 : Reconnaissance de plusieurs morceaux et recommandation**
 	
 	+ [**Fonctionnalité 4** : Un repertoire de fichiers audios et une petite interface graphique.](./S1_addmorceaux.md)
 	+ [**Fonctionnalité 5** : Un système de recommandation de morceaux .](./S1_recommandation.md)
 		

 	

### Objectif 2 : Constitution d'une bibliothèque musicale  (JOUR 2)

**<span style='color:red'>(A venir)</span>**

+  **Sprint 3** : **Concever et implémenter le modèle de votre base de données**
	+ [**Fonctionnalité 6** : Un modèle SQL avec SQLDesigner](./S3_model.md) 
	+  [**Fonctionnalité 7** : Prise en main de Django](./S3_model.md)
	+ **Fonctionnalité 7** : Création de notre modèle avec l'ORM Django
	    + Pour cela il faut aller [ici](https://gitlab-student.centralesupelec.fr/celine.hudelot/cs_coding_weeks_saclaycomptoirlocal/blob/master/S3_model.md)    

+ ...



