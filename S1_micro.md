# Fonctionnalité 3 : Prise en compte du micro


Ajouter dans le  package `audio_recognize`  un module permettant de reconnaître, à l'aide de la bibliothèque `dejavu` un fichier audio  provenant du micro.

Il ne s'agit ici que d'appliquer la bibliothèque existante.

Une fois terminée, passons alors à la [**Fonctionnalité 4** : Un repertoire de fichiers audios et une petite interface graphique.](./S1_addmorceaux.md)