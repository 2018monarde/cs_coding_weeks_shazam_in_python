
#Shazam - Sprint 0 : Analyse du problème

Une des premières étapes de tout travail de programmation et de développement logiciel, quelque soit la méthodologie de développement utilisée, consiste à réaliser une rapide **analyse des besoins**, avant toute phase d'implémentation.

Cette analyse a pour objectif d'identifier les principales fonctionnalités à developper pour avoir le comportement souhaité du système développé. Cette première liste de fonctionnalité n'a pas besoin d'être exhaustive ni figée mais elle vous permettra de construire vos premiers developpements.


## Analyse des besoins : les principales fonctionnalités

L'objectif ici est permettre à un utilisateur de soumettre un morceau de musique (un fichier audio) à une application en vue de sa reconnaissance et pour avoir ensuite une recommandation de morceaux à écouter. 

Le **[MVP (Minimum Viable product)](https://medium.com/creative-wallonia-engine/un-mvp-nest-pas-une-version-simplifi%C3%A9e-de-votre-produit-89017ac748b0)** de ce projet consistera à livrer une première version de l'application de reconnaissance avec les fonctionnalités de base permettant de répondre à l'objectif ci-dessus.

En particulier, le MVP : 

+ **Permettra à l'utilisateur de soumettre un morceau de musique venant de sa bibiothèque, ici un réperoire sur son disque**.
+ **Aura une petite base de morceaux de musique à sa disposition**
+ **Permettra la reconnaissance de ce morceau et retournera les informations relatives au morceau**.
+ **Permettra à l'utilisateur de sauvegarder le résultat de la reconnaissance.**
+ **Permettra à l'utilisateur de soumettre un morceau à partir du micro**
+ **...**

Finaliser ce travail pour avoir une vision claire de ce que sera votre MVP.

 
D'autres fonctionnalités pourront bien sûr être ajoutées ensuite pour améliorer votre projet après avoir fini ce MVP mais en construisant une chaîne de bout en bout, vous pourrez rapidement avoir des retours sur votre produit et ses fonctionnalités.

Vous pouvez maintenant continuez par le [Sprint 0 : Reflexion autour de la conception](./Sprint0Conception.md).